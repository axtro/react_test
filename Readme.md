# Prueba técnica de React

- [Prueba técnica de React](#prueba-técnica-de-react)
  - [Introducción](#introducción)
  - [Ejercicio Nivel 1: Interfaz y navegación](#ejercicio-nivel-1-interfaz-y-navegación)
  - [Ejercicio Nivel 2: Reproductor.](#ejercicio-nivel-2-reproductor)
  - [Ejercicio Nivel 3. Suscripción al servicio.](#ejercicio-nivel-3-suscripción-al-servicio)
  - [Notas](#notas)

## Introducción

Esta prueba técnica se divide en 3 ejercicios de creciente dificultad. Cada ejercicio depende del anterior, por lo que hay que hacerlos en orden.

Deben implementarse en React y se puede usar cualquier librería/framework que se necesite (create-react-app, nextjs, librerias de routing, estado, etc...).

Debe crearse un proyecto en git vacío y crear una rama independiente para cada ejercicio partiendo desde el ejercicio anterior.

Por ejemplo:

     | - master
         |- ejercicios/ejercicio1
            |- ejercicios/ejercicio2
               |- ejercicios/ejercicio3

De esta forma, la rama `ejercicios/ejercicios1` se creará a partir de `master`, la rama `ejercicios/ejercicio2` se creará a partir de la rama `ejercicios/ejercicio1` y la rama `ejercicios/ejercicio3` se creará a partir de la rama `ejercicios/ejercicio2`.

Esto es así porque cada ejercicio requiere modificar el anterior, pero es importante tener una rama de referencia a cada ejercicio independientemente.

---

## Ejercicio Nivel 1: Interfaz y navegación

El primer ejercicio consiste en el desarrollo de front de las páginas _Inicio_, _Listado de Clases_ y _Reproductor_ obteniendo varios datos desde el API, y navegación entre todas las pantallas.

El API está disponible aquí: https://bestcycling-public.s3.eu-west-1.amazonaws.com/api-test/db.json . Es un archivo estático, pero simula una respuesta de un API.

Los logos necesarios se encuentra en la carpeta `assets`.

El color principal es: #ff7900

Los colores de las habilidades son:

- Resistencia: #fcd900
- Fuerza: #f13b46
- Flexibilidad: #69ae00
- Mente: #1d8cb9

Los colores de las categorías son:

- Categoría 1: #ff7900
- Categoría 2: #8d88c5
- Categoría 11: #69ae00
- Categoría 21: #1d8cb9
- Categoría 31: #fcd900
- Categoría 41: #f13b46
- Resto de categorías: #fff

Requisitos:

- Implementar Pantalla de Inicio **lo más similar posible** a la siguiente imagen:

![Home](images/home.png)

Debe mostrar las **últimas 9 clases publicadas** que devuelva el API.
Al hacer click en una clase se accede al reproductor de la clase.
Al hacer click en "Ver todas" se accede al listado de clases.

- Implementar Pantalla de Listado de clases.

  ![Listado de clases](images/listado.png)

Debe mostrar todas las clases que devuelva el API ordenadas por fecha de publicación.
Al hacer click en una clase se accede al reproductor de la clase.

- Implementar Pantalla de reproducción

  ![Reproductor](images/player.png)

Esta apágina debe mostrar el título, nombre del instructor y el botón de volver a la página anterior.

---

## Ejercicio Nivel 2: Reproductor.

Este ejercicio consiste en desarrollar un _falso reproductor_ para poder "reproducir" las clases. También se debe modificar el listado para permitir seleccionar clases y reproducirlas de forma automática.

Requisitos:

- El reproductor ficticio consistirá en un contador decreciente de 5 segundos como en la siguiente imagen:

  ![Reproductor](images/player.png)

  Cuando la cuenta llegue a 0 debe volver a la página anterior automáticamente. Si se accedió a la clase desde el listado de clases se debe volver al listado de clase. Si se accedió desde la página de inicio se debe volver a la página de inicio.

  Se debe poder salir del reproductor en cualquier momento sin tener que esperar a que la cuenta llege a 0.

  Sólo si la reproducción de una clase finaliza, es decir, la cuenta atrás llega a 0, debe marcarse la clase como completada.

- El listado debe modificarse para incluir un checkbox de selección para poder selecionar una o varias clases:

  ![Selección de clases](images/seleccion-clases1.png)

- Si se selecciona una clase aparecerá el botón de "Iniciar reproducción" automáticamente.

  ![Selección clases](images/seleccion-clases2.png)

  Si el usuario selecciona varias clases, éstas deben reproducirse una tras otra, actualizando el título y el instructor de cada clase.

- En el listado debe aparecer la etiqueta en las clases que han sido completadas, es decir, las clases que se han reproducido totalmente.

  ![Reproductor](images/seleccion-clases2.png)

---

## Ejercicio Nivel 3. Suscripción al servicio.

Este ejercicio consiste en diseñar e implementar un sencillo sistema de suscripción. Sólo se puede reproducir una clase si el usuario está suscrito.

Para ello es necesario implementar una pequeña API que cree y valide las suscripciones.

El api debe ser lo más sencillo posible. No hay que persistir nada en base datos, sólo en memoria. Tampoco hay que implementar ningun tipo de login ni validar las peticiones. El cliente hará las llamadas con un ID de usuario y el backend funcionará con este ID.

Los endpoints y tipos de peticiones se pueden hacer según se prefiera.

Habrá 3 planes de suscripción: 1, 5 y 10 minutos. Si el usuario elige uno de estos planes éste estará suscrito durante el tiempo seleccionado.

Requisitos:

- Debe añadirse en el layout el estado de la suscripción.

  ![Layout suscripción](images/layout-suscripcion.png)

- Debe implementarse la página de suscripción. Los planes disponibles son la suscripción de 1, 5 y 10 minutos.

  ![Planes de suscripción](images/planes-suscripcion.png)

  Se debe añadir un checkbox en la página de suscripción para simular renovaciones automáticas.

- Debe diseñarse e implementarse el endpoint en el API que almacene información sobre el estado de la suscripción. (Debe controlarse que un usuario no pueda suscribirse más de una vez si hay una suscripción activa.

- Si el usuario está suscrito debe aparecer el tiempo de suscripción restante en el layout. Será una cuenta regresiva y al llegar a 0 debe cambiar el estado a "Suscribirse" o "Renovación pendiente" en caso de que el usuario haya marcado la autorenovación)

  ![Layout suscripción](images/layout-suscripcion1.png)

- La página de suscripción debe indicar el estado de la suscripción en caso de estar suscrito y cambiar la opción de autorenovación.

  ![Editar suscripción](images/editar-suscripcion.png)

- El acceso al reproductor sólo está diponible a los usuarios suscritos con suscripción activa.

Por tanto, al acceder al reproductor se pueden dar los siguientes casos:

1. El usuario está suscrito:
   Se iniciará la reproducción normalmente.

2. El usuario no esta suscrito.
   Se redijirá al usuario a la página de suscripción.

3. El usuario ha finalizado la suscripción pero tiene la opción de autorenovar activa.
   Se renovará la suscripción durante el periodo indicado por el usuario y se mostrará el reproductor.

Si el usuario está reproduciendo una clase y se termina el tiempo de suscripción durante la reproducción, el usuario debe poder reproducir la clase hasta el final.

- Por último, se debe controlar el caso en el que el tiempo de suscrición finalice durante la reproducción de una clase en una lista de reproducción. En este caso el usuario podrá reproducir la clase actual, no pudiendo reproducir la siguiente en caso de que el usuario no haya seleccionado la autorenovación. Si la ha seleccionado se extenderá un nuevo periodo de suscripción en cuanto se empiece a reproducir la siguiente clase en la lista.

## Notas

**[IMPORTANTE]**: Se debe usar componentes funcionales y hooks de react.
